class Fixnum
  def in_words
    ones = %w(zero one two three four five six seven eight nine)
    teens = %w(ten eleven twelve thirteen fourteen fifteen sixteen seventeen eighteen nineteen)
    tens = %w(nil ten twenty thirty forty fifty sixty seventy eighty ninety)

    result, int = "", self

    zeros = {1_000_000_000_000 => "trillion",
            1_000_000_000 => "billion",
            1_000_000 => "million",
            1000 => "thousand",
            100 => "hundred"}

    zeros.each do |k,v|
      next if int < k
      write = int / k
      result += " " if result.length > 0
      result += "#{write.in_words} #{v}"
      int = int % k
    end

    if int > 0
      result += " " if result.length > 0
      ten_num = int.to_s[0].to_i
      one_num = int.to_s[-1].to_i
      digits = int.to_s.length
      if digits == 2
        if one_num == 0
          result += "#{tens[ten_num]}"
        elsif ten_num == 1
          result += "#{teens[one_num]}"
        else
          result += "#{tens[ten_num]} #{ones[one_num]}"
        end
      else
        result += "#{ones[one_num]}"
      end
    end

    result += "#{ones[int]}" if int == 0 && result.empty?

    result
  end
end
